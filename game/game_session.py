import datetime

from utils.os_util import clearscreen


class GameSessionState(object):
    DEAD = "DEAD"
    SUCCESS = "SUCCESS"
    QUIT = "QUIT"
    SAVE = "SAVE"


class GameSession(object):
    """GameSession"""

    def __init__(self, store, **kwargs):
        super(GameSession, self).__init__()
        self.timestamp = datetime.datetime.now()
        self.pk = self.timestamp.strftime("%Y%m%d%H%M%S")
        self.store = store

        self.avatar = kwargs.get('avatar', None)
        self.name = kwargs.get('name', None)

        self.current_level_index = 0
        self.current_level = None

    def play(self):
        if self.avatar is None or self.name is None:
            self._choose_avatar()

        self._play_levels()

    def _save_state(self):
        # TODO
        pass

    def _choose_avatar(self):
        choice = -1
        while choice not in self.store.avatars:
            print "Choose an Avatar"
            for pk, avatar in self.store.avatars.iteritems():
                print "{}: {}".format(pk, avatar.name)
            choice = int(raw_input("Choice: "))
        self.avatar = self.store.avatars[choice]
        self.name = raw_input("Choose a name for you: ")

    def _play_levels(self):
        no_total_level = len(self.avatar.levels)
        for index in xrange(self.current_level_index, no_total_level):
            level = self.store.levels.get(self.avatar.levels[index])
            game_state = self._play_this_level(level)

            if game_state == GameSessionState.DEAD:
                self._hand_says("Sorry. You are dead")
                self._hand_says("We have to feed you to the Night King.")
                raw_input("Press any key")
                break

            elif game_state == GameSessionState.QUIT:
                break

            elif game_state == GameSessionState.SAVE:
                self._save_state()
                self._hand_says("Your state has been saved")
                raw_input("Press any key")
                break

            elif game_state == GameSessionState.SUCCESS:
                continue

    def _play_this_level(self, level):
        clearscreen()

        print "Hello {}, welcome to the next level.".format(self.name)
        print "Your hand, {}, is here to help you.".format(self.avatar.hand)

        print "Type `h [Enter]` for a clue."
        print "Type `q [Enter]` to quit the game."
        print "Type `4 b [Enter]` to put b in the 4th letter."

        no_of_lives = level.no_of_lives
        did_win = False
        question = level.question
        next_clue = 0

        while no_of_lives > 0 and did_win is False:
            print ""
            print "Character: {}".format(question)
            print "No. of Lives: {}/{}".format(no_of_lives, level.no_of_lives)
            answer = raw_input("Answer: ").strip()

            if answer == "h":
                if next_clue < len(level.clues):
                    self._hand_says(level.clues[next_clue])
                    next_clue += 1
                else:
                    self._hand_says("No more clues")
            elif answer == "q":
                self._hand_says(
                    "It is okay if you want to quit. Honour serving you.")
                raw_input("Press any key")
                return GameSessionState.QUIT
            elif answer == "s":
                return GameSessionState.SAVE
            else:
                result = self._find_answer(question, level.answer, answer)
                if result is False:
                    no_of_lives -= 1
                    self._hand_says("Wrong answer")
                else:
                    question = result

                if question.lower() == level.answer.lower():
                    did_win = True
                    self._hand_says("You won the level.")
                    self._hand_says("Let Us go to the next level.")
                    raw_input("Press any key")

        if no_of_lives == 0:
            return GameSessionState.DEAD
        else:
            return GameSessionState.SUCCESS

    def _hand_says(self, text):
        print "{} says: {}".format(self.avatar.hand, text)

    def _find_answer(self, question, orig_answer, answer):
        try:
            letter_index, char = answer.split()
            letter_index = int(letter_index) - 1
            char = char[0]

            if orig_answer[letter_index].lower() == char.lower():
                tmp = list(question)
                tmp[letter_index] = char
                question = "".join(tmp)
                return question
            else:
                return False
        except Exception as e:
            print "Invalid choice."
