from game.game_play import new_game

from utils.store import Store
from utils.os_util import clearscreen


store = Store()

menu_text = """
GAME OF THRONES - MENU
======================
1. New Game
2. Load a Game [Upcoming feature]
3. About
4. Quit

Choose (1-4):"""

about_text = """
About Game of Thrones
---------------------
- Wordman game based on the famous HBO Series - Game of Thrones
- You can choose among three emporers - Cersei, Khaleesi and Jon Snow
- Hand of the Emporer will be appointed accordingly
- At every level, you will have to guess a name of the character.
- Every level, you can ask the Hand to give as many clues as he/she has.
- Every level, you will have lives (max number of wrong tries).
- Finish up all your lives and you will be given to the Night King.
- Finish all levels to be crowned as the Head of Seven Kingdoms.
"""

thank_you_text = """
Thank you for playing Game of Thrones
Made with Love by Roopak A N
"""


def choose_menu_item():
    choice = None
    invalid_retries_remaining = 5
    while choice != 4 and invalid_retries_remaining > 0:
        try:
            clearscreen()
            choice = int(raw_input(menu_text))
            assert 1 <= choice <= 4

            if choice == 1:
                new_game(store)
            elif choice == 2:
                pass
            elif choice == 3:
                print about_text
                raw_input()
            elif choice == 4:
                print thank_you_text

        except Exception:
            print "Invalid choice"
            invalid_retries_remaining -= 1


if __name__ == '__main__':
    store.load_from_json_file("./db.json")
    choose_menu_item()
