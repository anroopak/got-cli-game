import os


def clearscreen(numlines=5):
    """Clear the console.
    numlines is an optional argument used only as a fall-back.
    """
    IS_DEBUG = os.getenv('IS_DEBUG', False)
    if IS_DEBUG is False and os.name == "posix":
        # Unix/Linux/MacOS/BSD/etc
        os.system('clear')
    elif IS_DEBUG is False and os.name in ("nt", "dos", "ce"):
        # DOS/Windows
        os.system('CLS')
    else:
        # Fallback for other operating systems.
        print('\n' * numlines)
