import json

from models.avatar import Avatar
from models.game_level import GameLevel

MODELS = {
    'avatars': Avatar,
    'levels': GameLevel
}


class Store(object):
    """Data store. Contains all the details in a hashmap / dict format"""

    def __init__(self):
        super(Store, self).__init__()
        self.avatars = {}
        self.levels = {}

    def load_from_json_file(self, path_to_json):
        """
        Loads data from the Json file passed
        and populates the store data

        Arguments:
                path_to_json {str} -- Path to the DB Json file
        """
        with open(path_to_json, "r") as db_json:
            all_info = json.load(db_json)
            for key, klass in MODELS.iteritems():
                self._load_model(key, klass, all_info.get(key, []))

    def _load_model(self, key, klass, model_list):
        for model in model_list:
            try:
                pk = model.pop('pk')
                obj = klass(pk, **model)
                attr = self.__getattribute__(key)
                attr[pk] = obj
                self.__setattr__(key, attr)
            except KeyError:
                print "Invalid JSON"
