# Game of Thrones - CLI Game

## About Game of Thrones
- Wordman game based on the famous HBO Series - Game of Thrones
- You can choose among three emporers - Cersei, Khaleesi and Jon Snow
- Hand of the Emporer will be appointed accordingly
- At every level, you will have to guess a name of the character.
- Every level, you can ask the Hand to give as many clues as he/she has.
- Every level, you will have lives (max number of wrong tries).
- Finish up all your lives and you will be given to the Night King.
- Finish all levels to be crowned as the Head of Seven Kingdoms.

## Requirements
- Python 2.7

## Installing Packages
- No external packages used.

## Running the Game
- `$ python index.py`

## Testing
- The approach taken is to test the feature as a whole. Hence, bash tests used. 
- If `raw_input` can be mocked from a file / list of strings, Python level testing is better. 

## Adding More Data
- All data required for the game is loaded from `./db.json`.
- Avatars, Game Levels can be created, edited and deleted from this JSON file. 
- `pk` value in each of the object should be unique. 
- `avatar.levels` configures which all levels should be used by that avatar.