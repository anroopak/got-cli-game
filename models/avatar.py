class Avatar(object):
    """Avatar"""

    def __init__(self, pk, **kwargs):
        super(Avatar, self).__init__()
        self.pk = pk
        self.name = kwargs.get('name')
        self.hand = kwargs.get('hand')
        self.levels = kwargs.get('levels')
