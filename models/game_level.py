class GameLevel(object):
    """Game Level"""

    def __init__(self, pk, **kwargs):
        super(GameLevel, self).__init__()
        self.pk = pk
        self.question = str(kwargs.get('question'))
        self.answer = kwargs.get('answer')
        self.clues = kwargs.get('clues')
        self.no_of_lives = kwargs.get('no_of_lives')
