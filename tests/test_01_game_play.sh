export IS_DEBUG=True

cd ../
python index.py < ./tests/test_01_input.txt > ./tests/test_01_output.txt
lines=$(cat ./tests/test_01_output.txt | egrep "Tyrion Lannister says: Let Us go to the next level." | wc -l)
if [[ $lines == "1" ]]; then
    echo "SUCCESS"
else
    echo "FAILED"
fi